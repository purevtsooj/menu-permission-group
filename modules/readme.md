Custom Menu Permission
============

Objective: To have a simple navigation menu system that shows different menu items to different user groups, and grants different access to pages for different user groups.

Requrements
-----------

* Asgard CMS v2
* User module of Asgard CMS
* Menu module of Asgard CMS
* To use `@cmp('menu-name')` tag instead of `@menu('menu-name')` tag in blade template.


Installation:
-------------

Install the module as according to all Drupal modules.

Copy into `[asgard-cms]/Modules/` directory.


Configuration
-------------
The configuration page is at Administer -> Permission Group -> Menu Permissions (/backend/cmp/menupermissions). If you need to create new permissions, you can use the 'Add new permission' tab on 'Permission Groups' page.

To reset a permission back to its original callback and value, choose 'default' for that permission, and save the page.

Preview
-------------

![](http://www.emholboo.mn/wp-content/uploads/2017/06/cmp-preview-a.gif)