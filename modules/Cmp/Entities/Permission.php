<?php

namespace Modules\Cmp\Entities;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $table = 'cmp__permissions';
    protected $fillable = [
    	'name',
    	'description'
    ];
}
