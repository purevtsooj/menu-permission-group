<?php

namespace Modules\Cmp\Entities;

use Illuminate\Database\Eloquent\Model;

class ConfigureTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'cmp__configure_translations';
}
