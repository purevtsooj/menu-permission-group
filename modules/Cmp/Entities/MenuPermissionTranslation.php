<?php

namespace Modules\Cmp\Entities;

use Illuminate\Database\Eloquent\Model;

class MenuPermissionTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'cmp__menupermission_translations';
}
