<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/cmp'], function (Router $router) {
    $router->bind('permission', function ($id) {
        return app('Modules\Cmp\Repositories\PermissionRepository')->find($id);
    });
    $router->get('permissions', [
        'as' => 'admin.cmp.permission.index',
        'uses' => 'PermissionController@index',
        'middleware' => 'can:cmp.permissions.index'
    ]);
    $router->get('permissions/create', [
        'as' => 'admin.cmp.permission.create',
        'uses' => 'PermissionController@create',
        'middleware' => 'can:cmp.permissions.create'
    ]);
    $router->post('permissions', [
        'as' => 'admin.cmp.permission.store',
        'uses' => 'PermissionController@store',
        'middleware' => 'can:cmp.permissions.create'
    ]);
    $router->get('permissions/{permission}/edit', [
        'as' => 'admin.cmp.permission.edit',
        'uses' => 'PermissionController@edit',
        'middleware' => 'can:cmp.permissions.edit'
    ]);
    $router->put('permissions/{permission}', [
        'as' => 'admin.cmp.permission.update',
        'uses' => 'PermissionController@update',
        'middleware' => 'can:cmp.permissions.edit'
    ]);
    $router->delete('permissions/{permission}', [
        'as' => 'admin.cmp.permission.destroy',
        'uses' => 'PermissionController@destroy',
        'middleware' => 'can:cmp.permissions.destroy'
    ]);



    $router->bind('configure', function ($id) {
        return app('Modules\Cmp\Repositories\ConfigureRepository')->find($id);
    });
    $router->get('configures', [
        'as' => 'admin.cmp.configure.index',
        'uses' => 'ConfigureController@index',
        'middleware' => 'can:cmp.permissions.index'
    ]);
    $router->post('configures', [
        'as' => 'admin.cmp.configure.store',
        'uses' => 'ConfigureController@store',
        'middleware' => 'can:cmp.permissions.create'
    ]);



    $router->bind('menupermission', function ($id) {
        return app('Modules\Cmp\Repositories\MenuPermissionRepository')->find($id);
    });
    $router->get('menupermissions', [
        'as' => 'admin.cmp.menupermission.index',
        'uses' => 'MenuPermissionController@index',
        'middleware' => 'can:cmp.permissions.index'
    ]);
    $router->post('menupermissions', [
        'as' => 'admin.cmp.menupermission.store',
        'uses' => 'MenuPermissionController@store',
        'middleware' => 'can:cmp.permissions.create'
    ]);
// append

});
