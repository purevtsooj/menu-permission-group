<?php

namespace Modules\Cmp\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Cmp\Entities\Permission;
use Modules\Cmp\Repositories\PermissionRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class PermissionController extends AdminBaseController
{
    /**
     * @var PermissionRepository
     */
    private $permission;

    public function __construct(PermissionRepository $permission)
    {
        parent::__construct();

        $this->permission = $permission;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $permissions = $this->permission->all();

        return view('cmp::admin.permissions.index', compact('permissions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('cmp::admin.permissions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->permission->create($request->all());

        return redirect()->route('admin.cmp.permission.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('cmp::permissions.title.permissions')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Permission $permission
     * @return Response
     */
    public function edit(Permission $permission)
    {
        return view('cmp::admin.permissions.edit', compact('permission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Permission $permission
     * @param  Request $request
     * @return Response
     */
    public function update(Permission $permission, Request $request)
    {
        $this->permission->update($permission, $request->all());

        return redirect()->route('admin.cmp.permission.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('cmp::permissions.title.permissions')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Permission $permission
     * @return Response
     */
    public function destroy(Permission $permission)
    {
        $this->permission->destroy($permission);

        return redirect()->route('admin.cmp.permission.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('cmp::permissions.title.permissions')]));
    }
}
