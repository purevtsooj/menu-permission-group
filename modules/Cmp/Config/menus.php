<?php

return [
	'navbar' =>	'Nwidart\Menus\Presenters\Bootstrap\NavbarPresenter',
	'navbar-right' => 'Nwidart\Menus\Presenters\Bootstrap\NavbarRightPresenter',
	'nav-pills' => 'Nwidart\Menus\Presenters\Bootstrap\NavPillsPresenter',
	'nav-tab' => 'Nwidart\Menus\Presenters\Bootstrap\NavTabPresenter',

	'cmp-menu'  =>  'Modules\Cmp\Presenters\CmpMenuPresenter',
];