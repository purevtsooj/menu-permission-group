<?php

namespace Modules\Cmp\Sidebar;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\User\Contracts\Authentication;

class SidebarExtender implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    /**
     * @param Menu $menu
     *
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        
        $menu->group(trans('core::sidebar.content'), function (Group $group) {
            $group->item('Permission Group', function (Item $item) {
                $item->icon('fa fa-cogs');
                $item->weight(30);
                $item->route('admin.cmp.permission.index');
                $item->authorize(
                    $this->auth->hasAccess('cmp.permissions.index')
                );

                $item->item('Menu Permissions', function (Item $item) {
                    $item->weight(0);
                    $item->icon('fa fa-columns');
                    $item->route('admin.cmp.menupermission.index');
                    $item->authorize(
                        $this->auth->hasAccess('cmp.permissions.index')
                    );
                });

                $item->item('Permission Groups', function (Item $item) {
                    $item->weight(1);
                    $item->icon('fa fa-cogs');
                    $item->route('admin.cmp.permission.index');
                    $item->authorize(
                        $this->auth->hasAccess('cmp.permissions.index')
                    );
                });

                $item->item('Configure', function (Item $item) {
                    $item->weight(2);
                    $item->icon('fa fa-users');
                    $item->route('admin.cmp.configure.index');
                    $item->authorize(
                        $this->auth->hasAccess('cmp.permissions.index')
                    );
                });

            });
        });

        return $menu;
    }
}
