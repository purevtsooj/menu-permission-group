<?php

namespace Modules\Cmp\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;

use Modules\Cmp\Blade\CmpDirective;

class CmpServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();

        $this->app->bind('cmp.directive', function () {
            return new CmpDirective();
        });
    }

    public function boot()
    {
        $this->registerBladeTags();
        // $this->publishConfig('cmp', 'settings');
        // $this->publishConfig('cmp', 'menus');
        $this->publishConfig('cmp', 'permissions');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Cmp\Repositories\PermissionRepository',
            function () {
                $repository = new \Modules\Cmp\Repositories\Eloquent\EloquentPermissionRepository(new \Modules\Cmp\Entities\Permission());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Cmp\Repositories\Cache\CachePermissionDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Cmp\Repositories\ConfigureRepository',
            function () {
                $repository = new \Modules\Cmp\Repositories\Eloquent\EloquentConfigureRepository(new \Modules\Cmp\Entities\Configure());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Cmp\Repositories\Cache\CacheConfigureDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Cmp\Repositories\MenuPermissionRepository',
            function () {
                $repository = new \Modules\Cmp\Repositories\Eloquent\EloquentMenuPermissionRepository(new \Modules\Cmp\Entities\MenuPermission());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Cmp\Repositories\Cache\CacheMenuPermissionDecorator($repository);
            }
        );
// add bindings
    }



    /**
     * Register menu blade tags
     */
    protected function registerBladeTags()
    {
        if (app()->environment() === 'testing') {
            return;
        }

        $this->app['blade.compiler']->directive('cmp', function ($arguments) {
            return "<?php echo CmpDirective::show([$arguments]); ?>";
        });
    }
}
