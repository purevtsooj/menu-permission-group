<?php

namespace Modules\Cmp\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use Modules\Cmp\Repositories\PermissionRepository;

class CmpDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call("OthersTableSeeder");

        $permission_group = app('Modules\Cmp\Repositories\PermissionRepository');

        $data = [
            'name' => 'Administrator permission',
            'description' => 'Collaboratively iterate quality functionalities whereas front-end mindshare.'
        ];
        $permission_group->create($data);

        $data = [
            'name' => 'Moderators permission',
            'description' => 'Collaboratively iterate quality functionalities whereas front-end mindshare.'
        ];
        $permission_group->create($data);

        $data = [
            'name' => 'User permission',
            'description' => 'Collaboratively iterate quality functionalities whereas front-end mindshare.'
        ];
        $permission_group->create($data);
    }
}
