<?php

namespace Modules\Cmp\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface PermissionRepository extends BaseRepository
{
}
