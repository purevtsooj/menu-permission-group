<?php

namespace Modules\Cmp\Repositories\Cache;

use Modules\Cmp\Repositories\ConfigureRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheConfigureDecorator extends BaseCacheDecorator implements ConfigureRepository
{
    public function __construct(ConfigureRepository $configure)
    {
        parent::__construct();
        $this->entityName = 'cmp.configures';
        $this->repository = $configure;
    }
}
