<?php

namespace Modules\Cmp\Repositories\Eloquent;

use Modules\Cmp\Repositories\MenuPermissionRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentMenuPermissionRepository extends EloquentBaseRepository implements MenuPermissionRepository
{
}
