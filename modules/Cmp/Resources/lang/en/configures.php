<?php

return [
    'list resource' => 'List configures',
    'create resource' => 'Create configures',
    'edit resource' => 'Edit configures',
    'destroy resource' => 'Destroy configures',
    'title' => [
        'configures' => 'Configure',
        'create configure' => 'Create a configure',
        'edit configure' => 'Edit a configure',
    ],
    'button' => [
        'create configure' => 'Create a configure',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
