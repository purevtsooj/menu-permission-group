<?php

return [
    'list resource' => 'List menupermissions',
    'create resource' => 'Create menupermissions',
    'edit resource' => 'Edit menupermissions',
    'destroy resource' => 'Destroy menupermissions',
    'title' => [
        'menupermissions' => 'MenuPermission',
        'create menupermission' => 'Create a menupermission',
        'edit menupermission' => 'Edit a menupermission',
    ],
    'button' => [
        'create menupermission' => 'Create a menupermission',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
