<?php

namespace Modules\Cmp\Entities;

use Illuminate\Database\Eloquent\Model;

class MenuPermission extends Model
{
    protected $table = 'cmp__menupermissions';
    protected $fillable = [
    	'menu_id',
    	'perm_id'
    ];
}
