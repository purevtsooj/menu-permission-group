<?php

namespace Modules\Cmp\Entities;

use Illuminate\Database\Eloquent\Model;

class PermissionTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'cmp__permission_translations';
}
