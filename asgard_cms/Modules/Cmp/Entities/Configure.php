<?php

namespace Modules\Cmp\Entities;

use Illuminate\Database\Eloquent\Model;

class Configure extends Model
{
    protected $table = 'cmp__configures';
    protected $fillable = [
    	'perm_id',
    	'role_id'
	];
}
