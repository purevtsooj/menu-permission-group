<?php

return [
    'list resource' => 'List permissions',
    'create resource' => 'Create permissions',
    'edit resource' => 'Edit permissions',
    'destroy resource' => 'Destroy permissions',
    'title' => [
        'permissions' => 'Permission',
        'create permission' => 'Create a permission',
        'edit permission' => 'Edit a permission',
    ],
    'button' => [
        'create permission' => 'Create a permission',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
