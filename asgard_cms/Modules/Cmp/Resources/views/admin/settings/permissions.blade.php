@extends('layouts.master')

@section('styles')
    <style>
        .cmp-table small{
    		display: block;
            opacity: 0.6;
        }
        .col-permission{
        	width: 230px;
        }
        .cmp-table td.text-center{
        	vertical-align: middle;
        }
        .cmp-table tbody tr:nth-child(even){
        	/*background-color: rgba(60,141,188, 0.05);*/
        }
        .cmp-table .menu-row{
        	background-color: rgba(60,141,188, 0.1);
        }
        .cmp-table .sub-menu-row td:first-child{
        	padding-left: 20px;
        }
    </style>
@stop

@section('content-header')
    <h1>Menu Permission</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">Menu Permission</li>
    </ol>
@stop

@section('content')
	{!! Form::open(['route' => ['admin.cmp.menupermission.store'], 'method' => 'post']) !!}
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                    <a href="{{ route('admin.cmp.permission.index') }}" class="btn btn-primary btn-flat" style="padding: 4px 10px;">
                        <i class="fa fa-pencil"></i> Create Permission Group
                    </a>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover cmp-table">
                            <thead>
                            <tr>
                                <th>Menu</th>
                                <th class="col-permission">Permission</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($menus as $m): ?>
                            <tr class="menu-row">
                                <td>
                                	<strong>{{ $m['title'] }}</strong>
                            		<small>{{ $m['name'] }}</small>
                            	</td>
                            	<td>&nbsp;</td>
                            </tr>
                            	<?php foreach ($m['children'] as $child): ?>
                            		<?php if( $child->is_root===0 ): ?>
                            		<tr class="sub-menu-row">
		                                <td>
		                                	{{ $child->title }}
		                            		<small>{{ $child->link_type != 'external' ? $child->locale . '/' . $child->uri : $child->url }}</small>
		                            	</td>
		                            	<td>
		                            		<select name="menu_perm_select[{{ $child->id }}]">
		                            			<option value="0">Default</option>
		                            			<?php foreach ($permissions as $permission): ?>
                                                    <?php
                                                    $selected = '';
                                                    $found = isset($model) && !empty($model) ? $model->findByAttributes( array('menu_id'=>$child->id) ) : null;
                                                    if( $found!==null && isset($found->perm_id) && $found->perm_id==$permission->id ){
                                                        $selected = 'selected="selected"';
                                                    }
                                                    ?>
		                            			<option value="{{ $permission->id }}" {{ $selected }}>{{ $permission->name }}</option>
		                            			<?php endforeach; ?>
		                            		</select>
		                            	</td>
		                            </tr>
		                        	<?php endif; ?>
                        		<?php endforeach; ?>
                            <?php endforeach; ?>
                            </tbody>
                            
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary btn-flat">Save Menu Permissions</button>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop

@section('scripts')
    <?php $locale = locale(); ?>
    <script type="text/javascript">
        $(function () {
            
        });
    </script>
@stop
