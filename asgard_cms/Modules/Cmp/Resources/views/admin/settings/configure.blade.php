@extends('layouts.master')

@section('styles')
    <style>
        .cmp-table small{
    		display: block;
            opacity: 0.6;
        }
        .checkbox-cell{
        	width: 100px;
        }
        .cmp-table td.text-center{
        	vertical-align: middle;
        }
        .cmp-table tbody tr:nth-child(even){
        	background-color: rgba(60,141,188, 0.05);
        }
    </style>
@stop

@section('content-header')
    <h1>Configure Permissions</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">Configure</li>
    </ol>
@stop

@section('content')
	{!! Form::open(['route' => ['admin.cmp.configure.store'], 'method' => 'post']) !!}
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover cmp-table">
                            <thead>
                            <tr>
                                <th>Permission</th>

                                <?php foreach ($roles as $role): ?>
                                	<th class="text-center checkbox-cell">{{ $role->name }}</th>
                                <?php endforeach; ?>
                                <th class="text-center checkbox-cell">Anonymous</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($permissions as $permission): ?>
                            <tr>
                                <td>
                            		{{ $permission->name }}
                            		<small>{{ $permission->description }}</small>
                            	</td>

                                <?php foreach ($roles as $role): ?>
                                	<td class="text-center">
                                        <?php
                                        $checked = '';
                                        if( isset($model) && !empty($model) ){
                                            $found = isset($model) && !empty($model) ? $model->findByAttributes( array('perm_id'=>$permission->id, 'role_id'=>$role->id) ) : null;
                                            if( $found!==null && isset($found->role_id) ){
                                                $checked = 'checked="checked"';
                                            }
                                        }
                                        ?>
                                		<input type="checkbox" name="perm_chbox_{{ $permission->id }}[]" value="{{ $role->id }}" {{ $checked }}>
                                	</td>
                                <?php endforeach; ?>

                                <td class="text-center">
                                    <?php
                                    $checked = '';
                                    if( isset($model) && !empty($model) ){
                                        $found = isset($model) && !empty($model) ? $model->findByAttributes( array('perm_id'=>$permission->id, 'role_id'=>0) ) : null;
                                        if( $found!==null && isset($found->role_id) ){
                                            $checked = 'checked="checked"';
                                        }
                                    }
                                    ?>
                                    <input type="checkbox" name="perm_chbox_{{ $permission->id }}[]" value="0" {{ $checked }}>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            </tbody>
                            
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary btn-flat">Save Permissions</button>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop

@section('scripts')
    <?php $locale = locale(); ?>
    <script type="text/javascript">
        $(function () {
            
        });
    </script>
@stop
