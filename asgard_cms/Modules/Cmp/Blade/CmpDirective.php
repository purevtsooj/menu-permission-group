<?php

namespace Modules\Cmp\Blade;

use Modules\Menu\Repositories\MenuRepository;
use Modules\Menu\Repositories\MenuItemRepository;
use Collective\Html\HtmlFacade as HTML;
use Modules\User\Contracts\Authentication;
use Modules\User\Repositories\RoleRepository;

use Modules\Cmp\Repositories\MenuPermissionRepository;
use Modules\Cmp\Repositories\ConfigureRepository;

final class CmpDirective
{
    private $name;
    private $presenter;
    private $bindings;

    public function show($arguments)
    {
        $this->extractArguments($arguments);

        return $this->returnMenu();
    }

    /**
     * Extract the possible arguments as class properties
     * @param array $arguments
     */
    private function extractArguments(array $arguments)
    {
        $this->name = array_get($arguments, 0);
        $this->presenter = array_get($arguments, 1);
        $this->bindings = array_get($arguments, 2, []);
    }

    /**
     * Prepare arguments and return menu
     * @return string|null
     */

    public function getActiveState($item, $state = ' class="active"')
    {
        return '';//$item->isActive() ? $state : null;
    }


    /**
     * Prepare arguments and return menu
     * @return string
     */
    public function getUrl($item)
    {
        return !empty($item->route) ? route($item->route[0], $item->route[1]) : url($item->url);
    }


    /**
     * Prepare arguments and return menu
     * @return string
     */
    public function getAttributes($item)
    {
        $attributes = $item->attributes ? $item->attributes : [];

        array_forget($attributes, ['active', 'icon']);

        return HTML::attributes($attributes);
    }


    /**
     * Prepare arguments and return menu
     * @return string
     */
    public function getIcon($item, $default = null)
    {
        return !is_null($item->icon) && $item->icon != "" ? '<i class="' . $item->icon . '"></i>' : $default;
    }


    /**
     * Prepare arguments and return menu
     * @return string
     */
    public function getMenuItem($item){
        return '<li' . $this->getActiveState($item) . '><a href="' . $this->getUrl($item) . '" ' . $this->getAttributes($item) . '>' . $this->getIcon($item) . ' ' . $item->title . '</a></li>' . PHP_EOL;
    }

    private function returnMenu()
    {

        $roles = app(RoleRepository::class);
        $roles = $roles->all();
        $current_roles = array(0);

        $auth = app(Authentication::class);
        $auth_check = $auth->check();
        if( $auth_check===true ){
            $user = $auth->user();

            foreach ($roles as $role) {
                if( $user->hasRoleId($role->id) ){
                    $current_roles[] = $role->id;
                }
            }
        }

        $result = '';

        $menus = app(MenuRepository::class);
        $menusItems = app(MenuItemRepository::class);

        $menus = $menus->allOnline();
        foreach ($menus as $menu) {
            if( $menu->name==$this->name ){
                $children = $menusItems->allRootsForMenu($menu->id);
                $children_html = '';
                foreach ($children as $item) {
                    if( $item->is_root===0 ){

                        // check permission group
                        $found = app('Modules\Cmp\Entities\MenuPermission')->where('menu_id', $item->id)->take(20)->get();
                        if( $found!==null && !empty($found) ){
                            $_roles = array();

                            foreach ($found as $match) {
                                $printable = false;
                                if( isset($match->perm_id) ){

                                    // check permission by role
                                    $found_conf = app('Modules\Cmp\Entities\Configure')->where('perm_id', $match->perm_id)->take(20)->get();
                                    if( $match->perm_id==0 ){
                                        $printable = true;
                                    }
                                    else if( $found_conf!==null ){
                                        foreach ($found_conf as $_conf) {
                                            if( isset($_conf->role_id) ){
                                                if(  in_array($_conf->role_id, $current_roles) ){
                                                    $printable = true;
                                                }
                                            }
                                        }
                                    }

                                }

                                if( $printable ){
                                    $children_html .= $this->getMenuItem($item);
                                }
                            }

                            if( count($found)==0 ){
                                $children_html .= $this->getMenuItem($item);
                            }

                        }
                        else{
                            $children_html .= $this->getMenuItem($item);
                        }
                    }
                }
                $result = sprintf('%s<ul class="nav navbar-nav">%s%s%s</ul>%s', PHP_EOL, PHP_EOL, $children_html, PHP_EOL, PHP_EOL);
            }
        }

        return $result;
        return app('menus')->get($this->name, $this->presenter, $this->bindings);
    }

    public function __toString()
    {
        return $this->returnMenu();
    }
}
