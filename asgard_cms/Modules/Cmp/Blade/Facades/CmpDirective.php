<?php

namespace Modules\Cmp\Blade\Facades;

use Illuminate\Support\Facades\Facade;

final class CmpDirective extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'cmp.directive';
    }
}
