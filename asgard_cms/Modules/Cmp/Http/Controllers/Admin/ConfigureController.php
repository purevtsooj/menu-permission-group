<?php

namespace Modules\Cmp\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Cmp\Entities\Configure;
use Modules\Cmp\Repositories\ConfigureRepository;
use Modules\Cmp\Repositories\PermissionRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

use Modules\User\Repositories\RoleRepository;

class ConfigureController extends AdminBaseController
{
    /**
     * @var ConfigureRepository
     */
    private $configure;

    public function __construct(ConfigureRepository $configure)
    {
        parent::__construct();

        $this->configure = $configure;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $model = $this->configure;
        $configures = $this->configure->all();

        // permission groups
        $permissions = app(PermissionRepository::class);
        $permissions = $permissions->all();

        // get user roles
        $roles = app(RoleRepository::class);
        $roles = $roles->all();

        return view('cmp::admin.settings.configure', compact('configures', 'permissions', 'roles', 'model'));
        // return view('cmp::admin.configures.index', compact('configures'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('cmp::admin.configures.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        // $this->configure->create($request->all());

        // truncate table
        $configures = $this->configure->all();
        foreach ($configures as $config) {
            $this->configure->destroy($config);
        }

        // collecting permission and roles
        $store = array();
        $permissions = app(PermissionRepository::class);
        $permissions = $permissions->all();
        foreach ($permissions as $perm) {
            $checkboxes = $request->get('perm_chbox_'.$perm->id);
            if( is_array($checkboxes) ){
                foreach ($checkboxes as $role) {
                    $store[] = array( 'perm_id'=>$perm->id, 'role_id'=>$role );
                }
            }
        }

        // save configures
        foreach ($store as $item) {
            $data = [
                'perm_id' => $item['perm_id'],
                'role_id' => $item['role_id']
            ];
            $this->configure->create($data);
        }

        return redirect()->route('admin.cmp.configure.index')
            ->withSuccess('Configures updated.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Configure $configure
     * @return Response
     */
    public function edit(Configure $configure)
    {
        return view('cmp::admin.configures.edit', compact('configure'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Configure $configure
     * @param  Request $request
     * @return Response
     */
    public function update(Configure $configure, Request $request)
    {
        $this->configure->update($configure, $request->all());

        return redirect()->route('admin.cmp.configure.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('cmp::configures.title.configures')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Configure $configure
     * @return Response
     */
    public function destroy(Configure $configure)
    {
        $this->configure->destroy($configure);

        return redirect()->route('admin.cmp.configure.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('cmp::configures.title.configures')]));
    }
}
