<?php

namespace Modules\Cmp\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Cmp\Entities\MenuPermission;
use Modules\Cmp\Repositories\MenuPermissionRepository;
use Modules\Cmp\Repositories\PermissionRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

use Modules\Menu\Repositories\MenuRepository;
use Modules\Menu\Repositories\MenuItemRepository;

class MenuPermissionController extends AdminBaseController
{
    /**
     * @var MenuPermissionRepository
     */
    private $menupermission;

    public function __construct(MenuPermissionRepository $menupermission)
    {
        parent::__construct();

        $this->menupermission = $menupermission;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $model = $this->menupermission;
        $menupermissions = $this->menupermission->all();

        // permission groups
        $permissions = app(PermissionRepository::class);
        $permissions = $permissions->all();

        // get menus
        $menu = app(MenuRepository::class);
        $menu = $menu->all();
        $menuItem = app(MenuItemRepository::class);

        // build menu structures
        $menus = array();
        foreach ($menu as $m) {
            $children = $menuItem->allRootsForMenu($m->id);
            $menus[] = array(
                'id' => $m->id,
                'title' => $m->title,
                'name' => $m->name,
                'children' => $children
            );
        }

        return view('cmp::admin.settings.permissions', compact('menupermissions', 'permissions', 'menus', 'model'));
        // return view('cmp::admin.menupermissions.index', compact('menupermissions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('cmp::admin.menupermissions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        // $this->menupermission->create($request->all());

        // truncate table
        $menupermissions = $this->menupermission->all();
        foreach ($menupermissions as $menu_perm) {
            $this->menupermission->destroy($menu_perm);
        }

        // save menu permissions
        $menu_permissions = $request->get('menu_perm_select');
        if( is_array($menu_permissions) ){
            foreach ($menu_permissions as $key => $value) {
                $data = [
                    'menu_id' => $key,
                    'perm_id' => $value
                ];
                $this->menupermission->create($data);
            }
        }

        return redirect()->route('admin.cmp.menupermission.index')
            ->withSuccess('Menu Permissions updated.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  MenuPermission $menupermission
     * @return Response
     */
    public function edit(MenuPermission $menupermission)
    {
        return view('cmp::admin.menupermissions.edit', compact('menupermission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  MenuPermission $menupermission
     * @param  Request $request
     * @return Response
     */
    public function update(MenuPermission $menupermission, Request $request)
    {
        $this->menupermission->update($menupermission, $request->all());

        return redirect()->route('admin.cmp.menupermission.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('cmp::menupermissions.title.menupermissions')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  MenuPermission $menupermission
     * @return Response
     */
    public function destroy(MenuPermission $menupermission)
    {
        $this->menupermission->destroy($menupermission);

        return redirect()->route('admin.cmp.menupermission.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('cmp::menupermissions.title.menupermissions')]));
    }
}
