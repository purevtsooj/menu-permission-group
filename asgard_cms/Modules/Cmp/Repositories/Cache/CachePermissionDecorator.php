<?php

namespace Modules\Cmp\Repositories\Cache;

use Modules\Cmp\Repositories\PermissionRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CachePermissionDecorator extends BaseCacheDecorator implements PermissionRepository
{
    public function __construct(PermissionRepository $permission)
    {
        parent::__construct();
        $this->entityName = 'cmp.permissions';
        $this->repository = $permission;
    }
}
