<?php

namespace Modules\Cmp\Repositories\Cache;

use Modules\Cmp\Repositories\MenuPermissionRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheMenuPermissionDecorator extends BaseCacheDecorator implements MenuPermissionRepository
{
    public function __construct(MenuPermissionRepository $menupermission)
    {
        parent::__construct();
        $this->entityName = 'cmp.menupermissions';
        $this->repository = $menupermission;
    }
}
