<?php

namespace Modules\Cmp\Repositories\Eloquent;

use Modules\Cmp\Repositories\ConfigureRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentConfigureRepository extends EloquentBaseRepository implements ConfigureRepository
{
}
