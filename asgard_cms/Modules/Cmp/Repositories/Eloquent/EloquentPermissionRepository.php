<?php

namespace Modules\Cmp\Repositories\Eloquent;

use Modules\Cmp\Repositories\PermissionRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentPermissionRepository extends EloquentBaseRepository implements PermissionRepository
{
}
