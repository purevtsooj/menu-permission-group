<?php


/*
use Modules\Cmp\Entities\Permission;

$permissions = Modules\Cmp\Entities\Permission::all();

foreach ($permissions as $perm) {
	error_log($perm->name);
}
*/


return [
    'cmp.permissions' => [
        'index' => 'cmp::permissions.list resource',
        'create' => 'cmp::permissions.create resource',
        'edit' => 'cmp::permissions.edit resource',
        'destroy' => 'cmp::permissions.destroy resource',
    ],
];
