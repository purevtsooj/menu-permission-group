<?php

return [
    'custom-menu-permission' => [
        'description' => 'core::settings.custom-menu-permission',
        'view' => 'text',
        'default' => ''
    ],
    'cmp-setting-permissions' => [
   		'description' => 'Permissions',
		'view' => 'cmp::admin.fields.permissions'
	]
];
