module.exports = function(grunt){

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    less: {
      compile: {
        options: {
          // sourceMap: true,
          // sourceMapFilename: 'css/default.css.map',
          // sourceMapURL: 'default.css.map',
          paths: ['assets/css'],
          modifyVars: {
            "img-path": "'../'"
          }
        },
        files: {
          'assets/css/main.css': 'resources/less/main.less'
        }
      }
    },

    // Build all files when Less file changes
    watch: {
      less: {
        files: ['resources/less/**/*.less'],
        tasks: ['less:compile']
      }
    }

  });
  
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-less-import-options');
  grunt.loadNpmTasks('grunt-contrib-watch');

  defined_tasks = [
    'less:compile',
    'watch'
  ];

  grunt.registerTask('default', defined_tasks);

}