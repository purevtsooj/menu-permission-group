Vagrant LAMP
============

My default LAMP development stack configuration for Vagrant.

Requrements
-----------

* Virtualbox
* Vagrant >= 1.7.0
* vagrant-omnibus plugin

Installation:
-------------

Download and install [VirtualBox](http://www.virtualbox.org/)

Download and install [vagrant](http://vagrantup.com/)

Install [vagrant-omnibus](https://github.com/chef/vagrant-omnibus) plugin

    $ vagrant plugin install vagrant-omnibus

Clone this repository

Go to the repository folder and launch the box

    $ cd [repo]
    $ vagrant up
    $ cd /vagrant/asgard_cms/
    $ php artisan asgard:install
    $ php artisan serve --host=192.168.10.10


What's inside:
--------------

Installed software:

* Apache
* MySQL
* php
* phpMyAdmin
* zsh
* git, subversion
* mc, vim, screen, tmux, curl
* [Composer](http://getcomposer.org/)
* Phing

Notes
-----

### Apache virtual hosts

You can add virtual hosts to apache by adding a file to the `data_bags/sites`
directory. The docroot of the new virtual host will be a directory within the
`public/` folder matching the `host` you specified. Alternately you may specify
a docroot explicitly by adding a `docroot` key in the json file.

### MySQL

The guests local 3306 port is available on the host at port 33066. It is available on every domain. Logging in can be done with username=root, password=vagrant.

### Composer

Composer binary is installed globally (to `/usr/local/bin`), so you can simply call `composer` from any directory.
