Custom Menu Permission
============

Objective: To have a simple navigation menu system that shows different menu items to different user groups, and grants different access to pages for different user groups.

I created Custom Menu Permissions (Cmp) module for Asgard CMS. [`/Modules/Cmp`]
It works with Menu and User modules of Asgard CMS. / Created menus and user roles

Requrements
-----------

* PHP 5.6+ or 7
* PHP Laravel 5.1+
* MySQL database

Installation:
-------------

Clone this repository

Go to the repository folder and launch the box

    $ cd [repo]
    $ cd asgard_cms/
    $ php artisan asgard:install
    $ php artisan serve


Cmp Module Requrements
----------------------

* Asgard CMS v2
* User module of Asgard CMS
* Menu module of Asgard CMS
* To use `@cmp('menu-name')` tag instead of `@menu('menu-name')` tag in blade template.

Module: Installation:
-------------

Install the module as according to all Drupal modules.

Copy into `[asgard-cms]/Modules/` directory.


Configuration
-------------
The configuration page is at Administer -> Permission Group -> Menu Permissions (/backend/cmp/menupermissions). If you need to create new permissions, you can use the 'Add new permission' tab on 'Permission Groups' page.

To reset a permission back to its original callback and value, choose 'default' for that permission, and save the page.

Preview
-------------

![](http://www.emholboo.mn/wp-content/uploads/2017/06/cmp-preview-a.gif)